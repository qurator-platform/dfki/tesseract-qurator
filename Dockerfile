FROM mauvilsa/tesseract-recognize:2020.01.13-ubuntu18.04-pkg

USER 0
EXPOSE 5000
ENTRYPOINT /usr/local/bin/tesseract_recognize_api.py --host 0.0.0.0
